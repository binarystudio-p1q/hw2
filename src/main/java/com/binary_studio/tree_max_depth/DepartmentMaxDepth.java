package com.binary_studio.tree_max_depth;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

public final class DepartmentMaxDepth {
	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		Deque<Department> DepartmentsQueue = new ArrayDeque<>();
		DepartmentsQueue.addLast(rootDepartment);
		Integer maxDepth = 0;

		while (DepartmentsQueue.size() > 0) {
			maxDepth++;
			int currentQueueSize = DepartmentsQueue.size();
			for (int i = 0; i < currentQueueSize; i++) {
				Department currentDepartment = DepartmentsQueue.removeFirst();
				currentDepartment.getSubDepartments()
						.stream()
						.filter(Objects::nonNull)
						.forEach(DepartmentsQueue::addLast);
			}
		}
		return maxDepth;
	}
}
