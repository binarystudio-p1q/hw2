package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {
	private final int initCapacitorAmount;
	private final int initShieldHP;
	private final int initHullHP;

	private String name;
	private PositiveInteger shieldHP;
	private PositiveInteger hullHP;
	private PositiveInteger powerGridOutput;
	private PositiveInteger capacitorAmount;
	private PositiveInteger capacitorRechargeRate;
	private PositiveInteger speed;
	private PositiveInteger size;
	private AttackSubsystem attackSubsystem;
	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
							PositiveInteger powerGridOutput, PositiveInteger capacitorAmount,
							PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size,
							AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.initCapacitorAmount = capacitorAmount.value();
		this.initShieldHP = shieldHP.value();
		this.initHullHP = hullHP.value();
	}

	@Override
	public void startTurn() {
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() + this.capacitorRechargeRate.value());
		if (this.capacitorAmount.value() > this.initCapacitorAmount) {
			this.capacitorAmount = PositiveInteger.of(this.initCapacitorAmount);
		}
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.attackSubsystem.getCapacitorConsumption().value() > this.capacitorAmount.value()) {
			return Optional.empty();
		}
		else {
			this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value()
					- this.attackSubsystem.getCapacitorConsumption().value());

			PositiveInteger damage = PositiveInteger.of(this.attackSubsystem.attack(target).value());
			AttackAction attackAction = new AttackAction(damage, this, target, this.attackSubsystem);

			return Optional.of(attackAction);
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		double reduceFactor = 1;
		if (this.defenciveSubsystem.getImpactReductionPercent().value() != 0) {
			reduceFactor = 100 / (double) this.defenciveSubsystem.getImpactReductionPercent().value();
		}

		int currentShieldHP = this.shieldHP.value() - attack.damage.value();
		int currentHullHP = this.hullHP.value();
		if (currentShieldHP < 0) {
			currentHullHP += currentShieldHP;
			currentShieldHP = 0;
		}

		if (currentHullHP > 0) {
			this.shieldHP = PositiveInteger.of(currentShieldHP);
			this.hullHP = PositiveInteger.of(currentHullHP);
			return new AttackResult.DamageRecived(attack.weapon,
					PositiveInteger.of((int) Math.ceil(attack.damage.value() / reduceFactor)), attack.target);
		}
		else {
			return new AttackResult.Destroyed();
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.defenciveSubsystem.getCapacitorConsumption().value() > this.capacitorAmount.value()) {
			return Optional.empty();
		}

		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value()
				- this.defenciveSubsystem.getCapacitorConsumption().value());

		int shieldRegenerateValue = this.defenciveSubsystem.getShieldRegeneration().value();
		int hullRegenerateValue = this.defenciveSubsystem.getHullRegeneration().value();

		int shieldRegenerateAmount = this.shieldHP.value() + shieldRegenerateValue > this.initShieldHP
				? this.initShieldHP - this.shieldHP.value() : shieldRegenerateValue;
		int hullRegenerateAmount = this.hullHP.value() + hullRegenerateValue > this.initHullHP
				? this.initHullHP - this.hullHP.value() : hullRegenerateValue;

		return Optional.of(new RegenerateAction(PositiveInteger.of(shieldRegenerateAmount),
				PositiveInteger.of(hullRegenerateAmount)));
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}
}
