package com.binary_studio.fleet_commander.core.exceptions;

public final class InsufficientPowergridException extends Exception {
	public InsufficientPowergridException(Integer missingPowergrid) {
		super(String.format("Missing %d MW to fit this module", missingPowergrid));
	}
}
