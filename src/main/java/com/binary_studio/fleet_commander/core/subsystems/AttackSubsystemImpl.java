package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
	private String name;
	private PositiveInteger powerGridConsumption;
	private PositiveInteger capacitorConsumption;
	private PositiveInteger optimalSpeed;
	private PositiveInteger optimalSize;
	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption,
										optimalSpeed, optimalSize, baseDamage);
	}

	public AttackSubsystemImpl(String name, PositiveInteger powerGridConsumption,
								PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed,
								PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powerGridConsumption = powerGridConsumption;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = (target.getSize().value() >= this.optimalSize.value())
				? 1 : ((double) target.getSize().value() / this.optimalSize.value());
		double speedReductionModifier = (target.getCurrentSpeed().value() <= this.optimalSpeed.value())
				? 1 : ((double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value()));
		double damage = this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);

		return new PositiveInteger(damage == 0 ? 0 : (int) Math.ceil(damage));
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getOptimalSpeed() {
		return this.optimalSpeed;
	}

	@Override
	public PositiveInteger getOptimalSize() {
		return this.optimalSize;
	}

	@Override
	public PositiveInteger getBaseDamage() {
		return this.baseDamage;
	}
}
