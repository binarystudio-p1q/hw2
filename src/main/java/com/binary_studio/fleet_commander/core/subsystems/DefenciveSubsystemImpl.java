package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {
	private String name;
	private PositiveInteger powerGridConsumption;
	private PositiveInteger capacitorConsumption;
	private PositiveInteger impactReductionPercent;
	private PositiveInteger shieldRegeneration;
	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption,
											impactReductionPercent, shieldRegeneration, hullRegeneration);
	}

	public DefenciveSubsystemImpl(String name, PositiveInteger powerGridConsumption,
									PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
									PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;
		this.powerGridConsumption = powerGridConsumption;
		this.capacitorConsumption = capacitorConsumption;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;

		if (impactReductionPercent.value() < 0) {
			this.impactReductionPercent = PositiveInteger.of(0);
		}
		else if (impactReductionPercent.value() > 95) {
			this.impactReductionPercent = PositiveInteger.of(95);
		}
		else {
			this.impactReductionPercent = impactReductionPercent;
		}
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		double reductionValue =  (double) incomingDamage.damage.value() * this.impactReductionPercent.value() / 100;
		int damage = (int) Math.ceil(incomingDamage.damage.value() - reductionValue);

		return new AttackAction(PositiveInteger.of(damage),
				incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger getImpactReductionPercent() {
		return this.impactReductionPercent;
	}

	@Override
	public PositiveInteger getShieldRegeneration() {
		return this.shieldRegeneration;
	}

	@Override
	public PositiveInteger getHullRegeneration() {
		return this.hullRegeneration;
	}
}
